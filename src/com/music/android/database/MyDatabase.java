package com.music.android.database;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.music.android.global.Global;
import com.music.android.utils.CommonUtils;

public class MyDatabase extends SQLiteOpenHelper {
	public static final String DATABASE_NAME = "database";
	public static final int DATABASE_VERSION = 1;

	public static final String USER_TABLE = "USERS";
	public static final String SONGS_TABLE = "songstable";
	public static final String SONG_DURATION_TABLE = "songdurationtable";

	public static final String UNSAFEWORD_PARENT_TABLE = "unsafe_parent_table";
	public static final String UNSAFEWORD_CHILD_TABLE = "unsafe_child_table";

	public static final String PLAYLIST_TABLE = "playlist_table";

	public static final String KEY_SONGSNAME = "song_name";
	public static final String KEY_SONGSPATH = "song_path";
	public static final String KEY_SONGSARTIST = "song_artist";
	public static final String KEY_SONGS_ID = "song_ID";
	public static final String KEY_SONGSALBUM_ID = "song_album_ID";
	public static final String KEY_SONGSTITLE = "song_title";
	public static final String KEY_SONGTABLEDELETEID = "delete_id";
	public static final String KEY_ALBUMNAME = "album_name";
	public static final String KEY_SONGS_PLAYLIST_NAME = "playlist_name";

	public static final String KEY_SCRUBEDSTATUS = "scrubed_status";
	public static final String KEY_ID = "id";
	public static final String DURATIONKEY_ID = "durationtable_id";
	public static final String KEY_START_DURATION = "start_duration";
	public static final String KEY_END_DURATION = "end_duration";
	public static final String KEY_TOTAL_DURATION = "total_duration";
	public static final String KEY_BADWORD_NAME = "bad_word_name";
	public static final String KEY_UNSAFEDID = "unsafed_id";
	public static final String KEY_DURATIONID = "duration_id";
	public static final String KEY_TABLEUNIQUEID = "unique_duration_id";

	public static final String KEY_PARENT_ID = "ID";
	public static final String KEY_PARENT_NAME = "Name";
	public static final String KEY_PARENT_UNSAFEWORDS = "Total_Unsafewords";
	public static final String KEY_PARENT_STATUS = "Status";
	public static final String KEY_PARENT_DELETE_STATUS = "Delete_Status";

	public static final String KEY_CHILD_ID = "ID";
	public static final String KEY_CHILD_PARENT_POSITION = "Parent_Position";
	public static final String KEY_CHILD_CATEGORY_ID = "CategoryID";
	public static final String KEY_CHILD_TITLE = "Name";
	public static final String KEY_CHILD_STATUS = "Status";
	public static final String KEY_CHILD_DELETE_STATUS = "Delete_Status";

	public static final String KEY_PLAYLIST_NAME = "NAME";
	public static final String KEY_PLAYLIST_SONGID = "SONG_ID";

	public static final String KEY_USER_ID = "USER_ID";
	public static final String KEY_EMAIL = "EMAIL";
	public static final String KEY_PASSWORD = "PASSWORD";
	public static final String KEY_USER_TYPE = "USER_TYPE";
	public static final String KEY_FILTER = "BLEEP_FILTER";
	public static final String KEY_OPTION = "BLEEP_OPTION";
	public static final String KEY_NOTIFICATION = "NOTIFICATION";

	MyDatabase database_music;
	static SQLiteDatabase db;
	private String temp_address;

	public MyDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		final String createSongsTable = "create table " + SONGS_TABLE + " ( " + KEY_SONGS_ID + " text PRIMARY KEY," + KEY_SONGSNAME + " text," + KEY_SONGSPATH + " text," + KEY_SONGSARTIST + " text," + KEY_SONGSALBUM_ID + " text," + KEY_SONGSTITLE + " text," + KEY_SCRUBEDSTATUS + " text," + KEY_ID + " text, " + KEY_SONGTABLEDELETEID + " text, " + KEY_ALBUMNAME + " text)";
		final String createUserTable = "CREATE TABLE " + USER_TABLE + " (" + KEY_USER_ID + " text PRIMARY KEY," + KEY_EMAIL + " text," + KEY_PASSWORD + " text," + KEY_USER_TYPE + " text," + KEY_FILTER + " text," + KEY_OPTION + " text," + KEY_NOTIFICATION + " text)";
		final String createSongsDurationTable = "create table " + SONG_DURATION_TABLE + " (" + KEY_TABLEUNIQUEID + " integer primary key autoincrement, " + DURATIONKEY_ID + " text," + KEY_START_DURATION + " text," + KEY_END_DURATION + " text," + KEY_DURATIONID + " text," + KEY_UNSAFEDID + " text," + KEY_TOTAL_DURATION + " text," + KEY_BADWORD_NAME + " text)";
		final String UNSAFEWORD_PARENT_TABLE_CREATE = "create table " + UNSAFEWORD_PARENT_TABLE + " (" + KEY_PARENT_ID + " text," + KEY_PARENT_NAME + " text," + KEY_PARENT_UNSAFEWORDS + " integer," + KEY_PARENT_STATUS + " text," + KEY_PARENT_DELETE_STATUS + " text," + KEY_USER_ID + " text)";
		final String UNSAFE_CHILD_TABLE_CREATE = "create table " + UNSAFEWORD_CHILD_TABLE + "(" + KEY_CHILD_ID + " text," + KEY_CHILD_TITLE + " text," + KEY_CHILD_STATUS + " text," + KEY_CHILD_PARENT_POSITION + " integer," + KEY_CHILD_DELETE_STATUS + " text," + KEY_CHILD_CATEGORY_ID + " TEXT," + KEY_USER_ID + " text)";
		final String createPlaylistTable = "create table " + PLAYLIST_TABLE + " (" + KEY_PLAYLIST_NAME + " text," + KEY_PLAYLIST_SONGID + " TEXT REFERENCES songstable(song_ID) ON UPDATE CASCADE ON DELETE CASCADE," + KEY_USER_ID + " text)";

		db.execSQL(createSongsTable);
		db.execSQL(createUserTable);
		db.execSQL(createSongsDurationTable);
		db.execSQL(UNSAFEWORD_PARENT_TABLE_CREATE);
		db.execSQL(UNSAFE_CHILD_TABLE_CREATE);
		db.execSQL(createPlaylistTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		CommonUtils.myLog("Database_Version", "oldVersion: " + oldVersion + "  newVersion: " + newVersion);

		if (newVersion > oldVersion) {
			/**
			 * SQLite supports a limited subset of ALTER TABLE.
			 * The ALTER TABLE command in SQLite allows the user to rename a table or to add a new column to an existing table.
			 * It is not possible to rename a column, remove a column, or add or remove constraints from a table.
			 * If you want to Drop/Rename a Column, You can:
			 * 
			 * create new table as the one you are trying to change,
			 * copy all data,
			 * drop old table,
			 * rename the new one.
			 */

			// use the below code if you want to ADD a new column in already existing TABLE.
			//						db.execSQL("ALTER TABLE " + SONGS_TABLE + " ADD COLUMN " + KEY_SONGS_PLAYLIST_NAME + " text ");

			// use the below code if you want to RENAME a already existing TABLE.
			//			db.execSQL("ALTER TABLE "+ old_tableName +" RENAME TO "+ new_tableName);

			// use the below code if you want to clean the entire Database.
			/*db.execSQL("DROP TABLE IF EXISTS " + SONGS_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + SONG_DURATION_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + UNSAFEWORD_PARENT_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + UNSAFEWORD_CHLID_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + PLAYLIST_TABLE);
			onCreate(db);*/
		}
	}

	public MyDatabase open() throws SQLException {
		db = this.getWritableDatabase();
		db.execSQL("PRAGMA foreign_keys=ON");
		return this;

	}

	//close the created database
	public void close() {
		db.close();
	}

	//insert values into the table : TABLENAME
	public long insert(String song_name, String song_path, String song_artist, String song_albumID, String song_ID, String song_title, String albumName, String deleteID) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_SONGS_ID, song_ID);
		initialValues.put(KEY_SONGSNAME, song_name);
		initialValues.put(KEY_SONGSPATH, song_path);
		initialValues.put(KEY_SONGSARTIST, song_artist);
		initialValues.put(KEY_SONGSALBUM_ID, song_albumID);
		initialValues.put(KEY_SONGSTITLE, song_title);
		initialValues.put(KEY_SONGTABLEDELETEID, deleteID);
		initialValues.put(KEY_ALBUMNAME, albumName);

		return db.insert(SONGS_TABLE, null, initialValues);
	}

	//insert values into the table : TABLENAME
	public boolean insertDeleteId(String song_title, String song_artist, String deleteID) {
		String mSongName = song_title.replace("'", "''");
		String mArtistName = song_artist.replace("'", "''");
		ContentValues args = new ContentValues();
		args.put(KEY_SONGTABLEDELETEID, deleteID);

		return db.update(SONGS_TABLE, args, KEY_SONGSTITLE + " LIKE " + "'" + mSongName + "' AND " + KEY_SONGSARTIST + " LIKE " + "'" + mArtistName + "'", null) > 0;

	}

	//fetch all songs
	public ArrayList<HashMap<String, String>> getAllSongs() {
		ArrayList<HashMap<String, String>> allSongList = new ArrayList<HashMap<String, String>>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + SONGS_TABLE + " ORDER BY " + KEY_SONGSTITLE + " ASC";

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> songMap = new HashMap<String, String>();
				songMap.put("songPath", cursor.getString(2));
				songMap.put("songArtist", cursor.getString(3));
				songMap.put("SongId", cursor.getString(0));
				songMap.put("albumId", cursor.getString(4));
				songMap.put("songTitle", cursor.getString(5));
				songMap.put("scrubStatus", cursor.getString(6));
				songMap.put("albumName", cursor.getString(9));

				allSongList.add(songMap);
			}
			while (cursor.moveToNext());
		}
		cursor.close();
		return allSongList;
	}

	//fetch all artists from database

	public ArrayList<String> fetchAllArtist() {
		ArrayList<String> allSongList = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT " + KEY_SONGSARTIST + " FROM " + SONGS_TABLE;

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				String artistName = cursor.getString(0);
				if (!allSongList.contains(artistName)) {

					allSongList.add(artistName);
				}

			}
			while (cursor.moveToNext());
		}
		cursor.close();

		return allSongList;
	}

	public ArrayList<String> fetchAllAlbums() {
		ArrayList<String> allSongList = new ArrayList<String>();
		// Select All Query
		String selectQuery = "SELECT " + KEY_ALBUMNAME + " FROM " + SONGS_TABLE;

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				String artistName = cursor.getString(0);
				if (!allSongList.contains(artistName)) {

					allSongList.add(artistName);
				}

			}
			while (cursor.moveToNext());
		}
		cursor.close();

		return allSongList;
	}

	//fetch song at selected position

	public ArrayList<HashMap<String, String>> fetchSong(String songName, String songArtist) {
		ArrayList<HashMap<String, String>> bleepedSongList = new ArrayList<HashMap<String, String>>();

		songName = songName.replace("'", "''");
		songArtist = songArtist.replace("'", "''");

		String fetchQuerymusicTable = "Select " + KEY_ID + " from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSTITLE + " LIKE " + "'%" + songName + "%' AND " + KEY_SONGSARTIST + " LIKE " + "'%" + songArtist + "%'";
		Cursor c = db.rawQuery(fetchQuerymusicTable, null);

		if (c.moveToFirst()) {
			temp_address = c.getString(0);
		}
		c.close();
		if (temp_address != null) {
			String fetchQuerySongTable = "Select * from " + SONG_DURATION_TABLE + " WHERE " + " " + DURATIONKEY_ID + " = \"" + temp_address + "\" ";
			Cursor songc = db.rawQuery(fetchQuerySongTable, null);

			if (songc.moveToFirst()) {
				do {
					HashMap<String, String> songMap = new HashMap<String, String>();
					songMap.put("id", songc.getString(0));
					songMap.put("startTime", songc.getString(2));
					songMap.put("endTime", songc.getString(3));
					songMap.put("unsafeId", songc.getString(5));
					songMap.put("totalduration", songc.getString(6));
					songMap.put("badWord", songc.getString(7));

					// Adding contact to list
					bleepedSongList.add(songMap);
				}
				while (songc.moveToNext());
			}
			songc.close();

		}

		return bleepedSongList;

	}

	//fetch songs according to artist

	public ArrayList<HashMap<String, String>> fetchSongsOfartist(String songArtist) {
		String mArtistName = songArtist.replace("'", "''");
		ArrayList<HashMap<String, String>> SongList = new ArrayList<HashMap<String, String>>();

		String fetchQuerymusicTable = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSARTIST + " LIKE " + "'" + mArtistName + "'";
		Cursor c = db.rawQuery(fetchQuerymusicTable, null);

		if (c.moveToFirst()) {
			do {
				HashMap<String, String> songMap = new HashMap<String, String>();
				songMap.put("songPath", c.getString(2));
				songMap.put("songArtist", c.getString(3));
				songMap.put("SongId", c.getString(0));
				songMap.put("albumId", c.getString(4));
				songMap.put("songTitle", c.getString(5));
				songMap.put("scrubStatus", c.getString(6));
				songMap.put("albumName", c.getString(9));
				// Adding song to list
				SongList.add(songMap);
			}
			while (c.moveToNext());

		}
		c.close();

		return SongList;

	}

	//fetch songs according to artist

	public ArrayList<HashMap<String, String>> fetchSongsOfAlbum(String albumName) {
		String mAlbumName = albumName.replace("'", "''");
		ArrayList<HashMap<String, String>> SongList = new ArrayList<HashMap<String, String>>();

		String fetchQuerymusicTable = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_ALBUMNAME + " LIKE " + "'" + mAlbumName + "'";
		Cursor c = db.rawQuery(fetchQuerymusicTable, null);

		if (c.moveToFirst()) {
			do {
				HashMap<String, String> songMap = new HashMap<String, String>();
				songMap.put("songPath", c.getString(2));
				songMap.put("songArtist", c.getString(3));
				songMap.put("SongId", c.getString(0));
				songMap.put("albumId", c.getString(4));
				songMap.put("songTitle", c.getString(5));
				songMap.put("scrubStatus", c.getString(6));
				songMap.put("albumName", c.getString(9));
				// Adding songs to list
				SongList.add(songMap);
			}
			while (c.moveToNext());

		}
		c.close();

		return SongList;

	}

	//public void updateColumn

	public void updateDeleteIDColumn() {
		ContentValues args = new ContentValues();
		args.put(KEY_SONGTABLEDELETEID, "1");
		db.update(SONGS_TABLE, args, null, null);
	}

	public boolean update(String song_artist, String song_name, String id, String scrubedStatus) {

		ContentValues args = new ContentValues();
		args.put(KEY_SCRUBEDSTATUS, scrubedStatus);
		args.put(KEY_ID, id);

		return db.update(SONGS_TABLE, args, KEY_SONGSTITLE + " LIKE " + "'" + song_name + "' AND " + KEY_SONGSARTIST + " LIKE " + "'" + song_artist + "'", null) > 0;

	}

	//insert values into the table : SONGTABLE

	public long insertSong(String id, String start_time, String end_time, String dur_Id, String unsafed_Id, String total_dur, String badWord) {

		ContentValues initValues = new ContentValues();
		initValues.put(DURATIONKEY_ID, id);
		initValues.put(KEY_START_DURATION, start_time);
		initValues.put(KEY_END_DURATION, end_time);
		initValues.put(KEY_DURATIONID, dur_Id);
		initValues.put(KEY_UNSAFEDID, unsafed_Id);
		initValues.put(KEY_TOTAL_DURATION, total_dur);
		initValues.put(KEY_BADWORD_NAME, badWord);

		return db.insert(SONG_DURATION_TABLE, null, initValues);

	}

	/**
	 * This method will insert values into unsafe_parent_table
	 * 
	 * @param ID
	 *            ID of parent unsafe words
	 * @param Name
	 *            Name of parent unsafe words
	 * @param Unsafeword
	 *            Total number of unsafe words
	 * @param Status
	 *            Activated or not
	 * @param userID
	 * @return TRUE if values inserted successfully, FALSE otherwise
	 */

	/**
	 * Checks whether this durationID already exists in songdurationtable or not.
	 * 
	 * @param durationID
	 * @return TRUE if durationID exists, FALSE otherwise
	 */
	public boolean durationIDExists(String durationID) {

		String query = "SELECT bad_word_name FROM songdurationtable WHERE duration_id = " + durationID;

		Cursor c = db.rawQuery(query, null);

		String badWord = null;

		if (c.moveToFirst()) {

			do {
				badWord = c.getString(0);

				if (badWord != null) {
					return true;
				}
			}
			while (c.moveToNext());

		}
		c.close();
		return false;

	}

	//delete song from database

	public void deleteSongs() {

		String fetchQuerymusicTable = "Select " + KEY_ID + " from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGTABLEDELETEID + "=" + "1";
		Cursor c = db.rawQuery(fetchQuerymusicTable, null);

		if (c.moveToFirst()) {

			do {
				temp_address = c.getString(0);

				if (temp_address != null) {
					db.delete(SONG_DURATION_TABLE, DURATIONKEY_ID + " = " + temp_address, null);
				}
			}
			while (c.moveToNext());

		}
		c.close();

		String getQuery = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGTABLEDELETEID + "=" + "1";
		Cursor getC = db.rawQuery(getQuery, null);

		db.delete(SONGS_TABLE, KEY_SONGTABLEDELETEID + "=" + "1", null);

		getC.close();

	}

	public int getScrubStatus(String songName, String songArtist) {
		int isScrubStatus = -1;

		songName = songName.replace("'", "''");
		songArtist = songArtist.replace("'", "''");

		String fetchQuerymusicTable = "Select " + KEY_SCRUBEDSTATUS + " from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSTITLE + " LIKE " + "'%" + songName + "%' AND " + KEY_SONGSARTIST + " LIKE " + "'%" + songArtist + "%'";
		Cursor c = db.rawQuery(fetchQuerymusicTable, null);

		if (c.moveToFirst()) {
			temp_address = c.getString(0);

		}
		c.close();

		if (temp_address != null) {
			if (temp_address.equals("3")) {
				isScrubStatus = 3;
			}
			else if (temp_address.equals("2") || temp_address.equals("5") || temp_address.equals("0")) {
				isScrubStatus = 2;
			}
		}
		else {
			isScrubStatus = -1;
		}

		return isScrubStatus;

	}

	public boolean getTitleSearch(String song_title) {
		song_title = song_title.replace("'", "''");

		String searchSongTitle = "Select " + KEY_SONGS_ID + " from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSTITLE + " LIKE " + "'%" + song_title + "%'";
		Cursor cursor = db.rawQuery(searchSongTitle, null);
		if (cursor.getCount() == 0) {
			cursor.close();
			return true;
		}
		else {
			cursor.close();
			return false;
		}
	}

	//search through list of songs
	public ArrayList<HashMap<String, String>> searchlist(String name, String tag) {
		String mName = name;
		ArrayList<HashMap<String, String>> SongList = new ArrayList<HashMap<String, String>>();

		Cursor c = null;
		if (tag.equalsIgnoreCase("songFragment")) {
			String fetchQuerymusicTable = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSTITLE + " LIKE " + "'%" + mName + "%'";
			c = db.rawQuery(fetchQuerymusicTable, null);
		}
		else if (tag.equalsIgnoreCase("artistFragment")) {
			String fetchQuerymusicTable = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_SONGSARTIST + " LIKE " + "'%" + mName + "%'";
			c = db.rawQuery(fetchQuerymusicTable, null);
		}
		else {
			String fetchQuerymusicTable = "Select * from " + SONGS_TABLE + " WHERE " + " " + KEY_ALBUMNAME + " LIKE " + "'%" + mName + "%'";
			c = db.rawQuery(fetchQuerymusicTable, null);
		}

		if (c.moveToFirst()) {
			do {
				HashMap<String, String> songMap = new HashMap<String, String>();
				songMap.put("songPath", c.getString(2));
				songMap.put("songArtist", c.getString(3));
				songMap.put("SongId", c.getString(0));
				songMap.put("albumId", c.getString(4));
				songMap.put("songTitle", c.getString(5));
				songMap.put("scrubStatus", c.getString(6));
				songMap.put("albumName", c.getString(9));
				// Adding songs to list
				SongList.add(songMap);
			}
			while (c.moveToNext());

		}
		c.close();

		return SongList;
	}

	// Getting song Count
	public int getSongCount() {
		String countQuery = "SELECT  * FROM " + SONGS_TABLE;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count = cursor.getCount();
		cursor.close();

		return count;
	}

	public static MyDatabase getDatabaseObj(Activity activity) {
		Global globalClass_obj = (Global) activity.getApplicationContext();
		MyDatabase myDatabase = globalClass_obj.myDb;
		return myDatabase;
	}

	/**
	 * This method is used to get the details of selected song.
	 * 
	 * @param songID
	 * @return
	 */
	public Cursor getSongDetails(String songID) {
		return db.rawQuery("SELECT * FROM songstable WHERE song_ID =?", new String[] { "" + songID });
	}

	//*************************************************************** UNSAFE WORDS ***************************************************************************************************

	/**
	 * This method will insert values into unsafe_parent_table
	 * 
	 * @param ID
	 *            ID of parent unsafe words
	 * @param Name
	 *            Name of parent unsafe words
	 * @param Unsafeword
	 *            Total number of unsafe words
	 * @param Status
	 *            Activated or not
	 * @param userID
	 * @return TRUE if values inserted successfully, FALSE otherwise
	 */
	public long insert_into_unsafe_parent_table(String ID, String Name, int Unsafeword, String Status, String delStatus, String userID) {
		ContentValues initValues = new ContentValues();
		initValues.put(KEY_PARENT_ID, ID);
		initValues.put(KEY_PARENT_NAME, Name);
		initValues.put(KEY_PARENT_UNSAFEWORDS, Unsafeword);
		initValues.put(KEY_PARENT_STATUS, Status);
		initValues.put(KEY_PARENT_DELETE_STATUS, delStatus);
		initValues.put(KEY_USER_ID, userID);

		return db.insert(UNSAFEWORD_PARENT_TABLE, null, initValues);
	}

	/**
	 * This method will insert values into unsafe_child_table
	 * 
	 * @param ID
	 *            ID of unsafe words
	 * @param CategoryID
	 *            Category of Unsafe words
	 * @param Title
	 *            Title of unsafe words
	 * @param Status
	 *            Activated or nor
	 * @param userID
	 * @return TRUE if values inserted successfully, FALSE otherwise
	 */
	public long insert_into_unsafe_child_table(String ID, String CategoryID, String Title, String Status, int parentPosition, String delStatus, String userID) {
		ContentValues initValues = new ContentValues();
		initValues.put(KEY_CHILD_ID, ID);
		initValues.put(KEY_CHILD_CATEGORY_ID, CategoryID);
		initValues.put(KEY_CHILD_TITLE, Title);
		initValues.put(KEY_CHILD_STATUS, Status);
		initValues.put(KEY_CHILD_PARENT_POSITION, parentPosition);
		initValues.put(KEY_CHILD_DELETE_STATUS, delStatus);
		initValues.put(KEY_USER_ID, userID);

		return db.insert(UNSAFEWORD_CHILD_TABLE, null, initValues);

	}

	/**
	 * This method will update the status of the selected parent.
	 * 
	 * @param id
	 *            id of unsafe word of parent table
	 * @param status
	 *            Activated or not
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean setParentStatus(String id, String status, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_PARENT_STATUS, status);
		return db.update(UNSAFEWORD_PARENT_TABLE, args, KEY_PARENT_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method will update the status of child(s) of the selected parent.
	 * 
	 * @param category_ID
	 *            category_ID of unsafe word of child table
	 * @param status
	 *            Activated or not
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean setChildStatus_Category(String category_ID, String status, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_CHILD_STATUS, status);
		return db.update(UNSAFEWORD_CHILD_TABLE, args, KEY_CHILD_CATEGORY_ID + " = " + category_ID + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method will update the status of the selected child.
	 * 
	 * @param id
	 *            id of unsafe words of child table
	 * @param status
	 *            Activated or not
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean setChildStatus(String id, String status, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_CHILD_STATUS, status);
		return db.update(UNSAFEWORD_CHILD_TABLE, args, KEY_CHILD_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method gives the details of the selected parent ID.
	 * 
	 * @param id
	 *            id of unsafe words of parentTable
	 * @param userID
	 * @return Details:
	 *         <p>
	 *         1 -> ID, 2 -> Name, 3 -> UnsafeWords size (integer), 4 -> Status
	 */
	public Cursor getParentDetails(String id, String userID) {

		String getStatus = "Select *  from " + UNSAFEWORD_PARENT_TABLE + " WHERE " + " " + KEY_PARENT_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID;
		Cursor c = db.rawQuery(getStatus, null);

		return c;
	}

	/**
	 * This method gives the details of the selected child ID.
	 * 
	 * @param id
	 *            id of unsafe words
	 * @param userID
	 * @return Unsafe words status
	 */
	public Cursor getChildDetails(String id, String userID) {

		String getStatus = "Select *  from " + UNSAFEWORD_CHILD_TABLE + " WHERE " + " " + KEY_CHILD_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID;
		Cursor c = db.rawQuery(getStatus, null);

		return c;
	}

	/**
	 * This method gives the status of selected bad word.
	 * 
	 * @param id
	 * @param userID
	 * @return
	 */
	public String getChildStatus(String id, String userID) {

		String getStatus = "Select " + KEY_CHILD_STATUS + " from " + UNSAFEWORD_CHILD_TABLE + " WHERE " + " " + KEY_CHILD_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID;
		Cursor c = db.rawQuery(getStatus, null);
		String temp_address = null;
		if (c.moveToFirst()) {

			temp_address = c.getString(0);
		}
		c.close();
		return temp_address;
	}

	/**
	 * This method gives the details of all unsafe words correspond to particular category ID
	 * 
	 * @param category_ID
	 *            category Id of unsafe words
	 * @param userID
	 * @return Unsafe word status
	 */
	public Cursor getChildDetails_Category(String category_ID, String userID) {

		String getStatus = "Select *  from " + UNSAFEWORD_CHILD_TABLE + " WHERE " + " " + KEY_CHILD_CATEGORY_ID + " = " + category_ID + " AND " + KEY_USER_ID + " = " + userID;
		Cursor c = db.rawQuery(getStatus, null);

		return c;
	}

	/**
	 * This method will update the details of the selected parent.
	 * 
	 * @param id
	 *            id of unsafe word of parent table
	 * @param name
	 *            parentName
	 * @param unsafeWords
	 *            size of unsafeWords
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updateParentDetails(String id, String name, int unsafeWords, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_PARENT_NAME, name);
		args.put(KEY_PARENT_UNSAFEWORDS, unsafeWords);
		return db.update(UNSAFEWORD_PARENT_TABLE, args, KEY_PARENT_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method will update the title of the selected child.
	 * 
	 * @param id
	 *            id of unsafe words of child table
	 * @param title
	 *            chlidName
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updateChildDetails(String id, String title, int parentPosition, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_CHILD_TITLE, title);
		args.put(KEY_CHILD_PARENT_POSITION, parentPosition);
		return db.update(UNSAFEWORD_CHILD_TABLE, args, KEY_CHILD_ID + " = " + id + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method is used to get all the rows from unsafe_parent_table.
	 * 
	 * @param userID
	 * @return
	 */
	public Cursor getUnsafeParentTable(String userID) {
		String getUnsafeTable = "SELECT * FROM " + UNSAFEWORD_PARENT_TABLE + " WHERE " + KEY_USER_ID + " = " + userID;
		Cursor c = db.rawQuery(getUnsafeTable, null);

		return c;
	}

	/**
	 * This method will update the Delete_Status of unsafe_parent_table.
	 * 
	 * @param ID
	 * @param deleteStatus
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updateParentDeleteStatus(String ID, String deleteStatus, String userID) {
		ContentValues args = new ContentValues();
		args.put(KEY_PARENT_DELETE_STATUS, deleteStatus);
		return db.update(UNSAFEWORD_PARENT_TABLE, args, KEY_PARENT_ID + " = " + ID + " AND " + KEY_USER_ID + " = " + userID, null) > 0;
	}

	/**
	 * This method will update the Delete_Status of all the child's of the selected parent within unsafe_child_table.
	 * 
	 * @param categoryID
	 * @param deleteStatus
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updateAllChildDeleteStatus(String categoryID, String deleteStatus, String userID) {
		ContentValues args = new ContentValues();
		args.put(KEY_CHILD_DELETE_STATUS, deleteStatus);
		return db.update(UNSAFEWORD_CHILD_TABLE, args, KEY_CHILD_CATEGORY_ID + " = " + categoryID + " AND " + KEY_USER_ID + " = " + userID, null) > 0;
	}

	/**
	 * This method will update the Delete_Status of selected child within unsafe_child_table.
	 * 
	 * @param ID
	 * @param deleteStatus
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updateChildDeleteStatus(String ID, String deleteStatus, String userID) {
		ContentValues args = new ContentValues();
		args.put(KEY_CHILD_DELETE_STATUS, deleteStatus);
		return db.update(UNSAFEWORD_CHILD_TABLE, args, KEY_CHILD_ID + " = " + ID + " AND " + KEY_USER_ID + " = " + userID, null) > 0;
	}

	/**
	 * This method will delete the rows from unsafe_parent_table with delete_status = Yes.
	 * 
	 * @param deleteStatus
	 * @param userID
	 * @return Number of rows effected if the values are deleted successfully, 0 otherwise
	 */
	public int deleteParent(String userID) {
		return db.delete(UNSAFEWORD_PARENT_TABLE, KEY_PARENT_DELETE_STATUS + " = ?" + " AND " + KEY_USER_ID + " = ?", new String[] { "Yes", "" + userID });
	}

	/**
	 * This method will delete the rows from unsafe_child_table with delete_status = Yes.
	 * 
	 * @param deleteStatus
	 * @param userID
	 * @return Number of rows effected if the values are deleted successfully, 0 otherwise
	 */
	public int deleteChild(String userID) {
		return db.delete(UNSAFEWORD_CHILD_TABLE, KEY_CHILD_DELETE_STATUS + " = ?" + " AND " + KEY_USER_ID + " = ?", new String[] { "Yes", "" + userID });
	}

	//********************************************************************* PLAY LIST ************************************************************************************

	/**
	 * This method is used to create a new play list.
	 * 
	 * @author ngoyal
	 * @param Name
	 *            Name of the Play list
	 * @param songID
	 *            ID of the Song to be added into the Play list
	 * @param userID
	 * @return the row ID of the newly inserted row, or -1 if an error occurred.
	 */
	public long insert_into_playlist_table(String Name, String songID, String userID) {
		ContentValues args = new ContentValues();
		args.put(KEY_PLAYLIST_NAME, Name);
		args.put(KEY_PLAYLIST_SONGID, songID);
		args.put(KEY_USER_ID, userID);

		return db.insert(PLAYLIST_TABLE, null, args);

	}

	/**
	 * This method will delete the selected Play list from the table.
	 * 
	 * @author ngoyal
	 * @param Name
	 *            Name of the play list to be deleted.
	 * @param userID
	 * @return Number of rows effected if the values are deleted successfully, 0 otherwise
	 */
	public int deletePlaylist(String Name, String userID) {
		return db.delete(PLAYLIST_TABLE, KEY_PLAYLIST_NAME + " = ?" + " AND " + KEY_USER_ID + " = ?", new String[] { "" + Name, "" + userID });
	}

	/**
	 * This method will return the name of all Play lists.
	 * 
	 * @author ngoyal
	 * @param userID
	 * @return
	 */
	public Cursor getAllPlaylists(String userID) {

		return db.rawQuery("Select * from playlist_table WHERE " + KEY_USER_ID + " = " + userID, null);
	}

	/**
	 * This method will get all the songs of the selected play list.
	 * 
	 * @author ngoyal
	 * @param playListName
	 * @param userID
	 * @return
	 */
	public Cursor getPlaylistSongs(String playListName, String userID) {
		return db.rawQuery("Select * from songstable INNER JOIN playlist_table ON songstable.song_ID=playlist_table.SONG_ID WHERE playlist_table.NAME =?" + " AND playlist_table.USER_ID = ?", new String[] { "" + playListName, "" + userID });
	}

	/**
	 * This method will delete a particular Song from the selected Play list.
	 * 
	 * @author ngoyal
	 * @param playlistName
	 * @param songID
	 * @param userID
	 * @return TRUE if the values are deleted successfully, FALSE otherwise
	 */
	public boolean deleteSongsFromPlaylist(String playlistName, String songID, String userID) {

		return db.delete("playlist_table", "NAME = " + "'" + playlistName + "'" + " AND SONG_ID = " + songID + " AND " + KEY_USER_ID + " = " + userID, null) > 0;
	}

	/**
	 * This method will update the name of selected Play list.
	 * 
	 * @author ngoyal
	 * @param oldName
	 * @param newName
	 * @param userID
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updatePlaylistName(String oldName, String newName, String userID) {

		ContentValues args = new ContentValues();
		args.put(KEY_PLAYLIST_NAME, newName);
		return db.update("playlist_table", args, " NAME = " + oldName + " AND " + KEY_USER_ID + " = " + userID, null) > 0;

	}

	/**
	 * This method will return the list of all the Songs which are not present in the selected Play list.
	 * 
	 * @author ngoyal
	 * @param playlistName
	 * @param userID
	 * @return
	 */
	public Cursor getNewSongsForPlaylist(String playlistName, String userID) {

		return db.rawQuery("SELECT * FROM songstable WHERE song_ID NOT IN(SELECT SONG_ID FROM playlist_table WHERE NAME =?" + " AND " + KEY_USER_ID + " = ?)", new String[] { "" + playlistName, "" + userID });
	}

	/********************************************************************************************************************************************************************************/

	/**
	 * Update scrub status of the selected song.
	 * 
	 * @param srcubStatus
	 * @param songID
	 * @return
	 */
	public static boolean updateSrcubStatus(String srcubStatus, String songID) {

		ContentValues args = new ContentValues();
		args.put(KEY_SCRUBEDSTATUS, srcubStatus);
		return db.update("songstable", args, " song_ID = " + songID, null) > 0;

	}

	/************************************************************************* USER *********************************************************************/

	/**
	 * This method is used to insert values into USERS table.
	 * 
	 * @param userID
	 * @param email
	 * @param password
	 * @param userType
	 *            whether the user is simple user or facebook user.
	 * @param filter
	 *            whether the bleep filter is ON or OFF.
	 * @param option
	 *            which bleep option is activated, Skip or Silence.
	 * @param notification
	 *            whether the notifications are ON or OFF.
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	public long insertIntoUserTable(String userID, String email, String password, String userType, String filter, String option, String notification) {
		ContentValues values = new ContentValues();
		values.put(KEY_USER_ID, userID);
		values.put(KEY_EMAIL, email);
		values.put(KEY_PASSWORD, password);
		values.put(KEY_USER_TYPE, userType);
		values.put(KEY_FILTER, filter);
		values.put(KEY_OPTION, option);
		values.put(KEY_NOTIFICATION, notification);

		return db.insert(USER_TABLE, null, values);
	}

	/**
	 * This method will check whether the specific user already exists or not.
	 * 
	 * @param userID
	 * @return
	 */
	public Cursor checkUser(String userID) {
		return db.rawQuery("SELECT * FROM USERS WHERE USER_ID=?", new String[] { "" + userID });

	}

	/**
	 * This method will provide the userID, email and password.
	 * 
	 * @param emailID
	 * @return
	 */
	public Cursor getUserDetails(String emailID) {
		return db.rawQuery("SELECT USER_ID, EMAIL, PASSWORD FROM USERS WHERE EMAIL=?", new String[] { "" + emailID });
	}

	/************************************************************************* SETTINGS *********************************************************************/

	/**
	 * This method is used to get the settings of a specific user.
	 * 
	 * @param userID
	 * @return BLEEP_FILTER, BLEEP_OPTION, NOTIFICATION
	 */
	public Cursor getBleepSettings(String userID) {
		return db.rawQuery("SELECT BLEEP_FILTER,  BLEEP_OPTION, NOTIFICATION FROM USERS WHERE USER_ID=?", new String[] { "" + userID });
	}

	/**
	 * This method will update the user's password.
	 * 
	 * @param userID
	 * @param newPassword
	 * @return TRUE if values updated successfully, FALSE otherwise
	 */
	public boolean updatePassword(String userID, String newPassword) {
		ContentValues values = new ContentValues();
		values.put(KEY_PASSWORD, newPassword);
		return db.update("USERS", values, " USER_ID = " + userID, null) > 0;
	}

	/**
	 * This method will update the bleep filter status for specific user.
	 * 
	 * @param userID
	 * @param bleepFilter
	 * @return
	 */
	public boolean updateBleepFilter(String userID, String bleepFilter) {
		ContentValues values = new ContentValues();
		values.put(KEY_FILTER, bleepFilter);
		return db.update("USERS", values, " USER_ID = " + userID, null) > 0;
	}

	/**
	 * This method will update the bleep option status for specific user.
	 * 
	 * @param userID
	 * @param bleepOption
	 * @return
	 */
	public boolean updateBleepOption(String userID, String bleepOption) {
		ContentValues values = new ContentValues();
		values.put(KEY_OPTION, bleepOption);
		return db.update("USERS", values, " USER_ID = " + userID, null) > 0;
	}

	/**
	 * This method will update the notification status for specific user.
	 * 
	 * @param userID
	 * @param notificationStatus
	 * @return
	 */
	public boolean updateNotificationStatus(String userID, String notificationStatus) {
		ContentValues values = new ContentValues();
		values.put(KEY_NOTIFICATION, notificationStatus);
		return db.update("USERS", values, " USER_ID = " + userID, null) > 0;
	}

}
